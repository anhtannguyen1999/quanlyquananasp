USE [master]
GO
/****** Object:  Database [DBQUANLYQUANAN]    Script Date: 6/6/2020 7:11:40 AM ******/
CREATE DATABASE [DBQUANLYQUANAN]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBQUANLYQUANAN', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\DBQUANLYQUANAN.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DBQUANLYQUANAN_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\DBQUANLYQUANAN_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DBQUANLYQUANAN] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBQUANLYQUANAN].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBQUANLYQUANAN] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET  MULTI_USER 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBQUANLYQUANAN] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBQUANLYQUANAN] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [DBQUANLYQUANAN]
GO
/****** Object:  StoredProcedure [dbo].[Kh_Account_Login]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Kh_Account_Login]
	@UserName varchar(50),
	@Password varchar(32)
as
begin
	declare @count int
	declare @res int
	select @count =count(*) from Account where TAIKHOAN=@UserName and MATKHAU=@Password and LOAIUSER='kh'
	if @count>0
		set @res=1
	else
		set @res=0
	select @res 
end
GO
/****** Object:  StoredProcedure [dbo].[Nv_Account_Login]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--KIEM TRA DANG NHAP NHANVIEN--
create proc [dbo].[Nv_Account_Login]
	@UserName varchar(50),
	@Password varchar(32)
as
begin
	declare @count int
	declare @res int
	select @count =count(*) from Account where TAIKHOAN=@UserName and MATKHAU=@Password and LOAIUSER='nv'
	if @count>0
		set @res=1
	else
		set @res=0
	select @res 
end



GO
/****** Object:  StoredProcedure [dbo].[Sp_ACCOUNT_GetByID]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_ACCOUNT_GetByID]
@ID varchar(10)
as
begin
	select* from ACCOUNT where ID=@ID
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_ACCOUNT_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_ACCOUNT_Insert] @id varchar(10), @hoten nvarchar(50) , @username varchar(50) ,@pass varchar(32)
as
begin
	begin
	insert into ACCOUNT(ID,TAIKHOAN,MATKHAU,HOTEN,NGAYSINH,GIOITINH,SDT,DIACHI,LOAIUSER) 
	values (@id,@username,@pass,@hoten,'1/1/1990',N'Nam','',N'','kh')
	end
	begin
	insert into KHACHHANG(MAKHACHHANG, DIACHINHANHANG)
	values (@id,N'')
	end 
end



--INSERT KHACHHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_Account_Login]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--KIEM TRA DANG NHAP ADMIN--
create proc [dbo].[Sp_Account_Login]
	@UserName varchar(50),
	@Password varchar(32)
as
begin
	declare @count int
	declare @res int
	select @count =count(*) from Account where TAIKHOAN=@UserName and MATKHAU=@Password
	if @count>0
		set @res=1
	else
		set @res=0
	select @res 
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_ACCOUNT_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_ACCOUNT_Update] @userID varchar(10), @hoTen nvarchar(50),
	@gioiTinh nvarchar(3), @ngaySinh smalldatetime, @sdt varchar(11),
	@taiKhoan varchar(50) , @diaChi nvarchar(150)
as
begin
	UPDATE ACCOUNT SET
	HOTEN=@hoTen, GIOITINH=@gioiTinh,
	NGAYSINH=@ngaySinh, SDT=@sdt,
	TAIKHOAN=@taiKhoan, DIACHI=@diaChi
	where ID=@userID
end

--UPDATE KHACHHANG DIACHINHANHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_ACCOUNT_UpdateTK]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_ACCOUNT_UpdateTK] @id varchar(10),@taiKhoan varchar(50), @matKhau varchar(32)
as
begin
	UPDATE ACCOUNT
	set TAIKHOAN=@taiKhoan, MATKHAU=@matKhau
	where ID=@id
end


--UPDATE CHAMCONGNHANVIEN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_DemSoDonChuaXacNhan]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_DemSoDonChuaXacNhan] @ngay smallint,@thang smallint, @nam smallint
as
begin
	select count(*) from DONDATHANG where day(NGAY)=@ngay and MONTH(NGAY)=@thang and YEAR(NGAY)=@nam and TINHTRANGDONHANG=0
end



----------------------

		
	
		

--GET TONG THU TAI QUAN 1 NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetBXHMonAn]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetBXHMonAn] @thang smallint,@nam smallint
as
begin
	select TOP 10 MAMATHANG, SUM(SOLUONGDABAN) as TONGDABAN from
		(select * from
		(select MAMATHANG,SUM(SOLUONG) as SOLUONGDABAN from CT_DONGOIMON inner join DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
		where TINHTRANG=1 and MAMATHANG like 'MA%' and month(NGAY)=@thang and year(NGAY)=@nam 
		group by MAMATHANG)BANGTAIQUAN
		union
		(select MAMATHANG,SUM(SOLUONG) as SOLUONGDABAN from CT_DONDATHANG inner join DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
		where TINHTRANGDONHANG=2 and MAMATHANG like 'MA%' and month(NGAY)=@thang and year(NGAY)=@nam 
		group by MAMATHANG)
		)BANGTONGHOP
	group by BANGTONGHOP.MAMATHANG
	order by TONGDABAN desc
end
--xep hang nuoc giai khat da ban

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetBXHNuocGiaiKhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetBXHNuocGiaiKhat] @thang smallint,@nam smallint
as
begin
	select TOP 10 MAMATHANG, SUM(SOLUONGDABAN) as TONGDABAN from
		(select * from
		(select MAMATHANG,SUM(SOLUONG) as SOLUONGDABAN from CT_DONGOIMON inner join DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
		where TINHTRANG=1 and MAMATHANG like 'NC%' and month(NGAY)=@thang and year(NGAY)=@nam 
		group by MAMATHANG)BANGTAIQUAN
		union
		(select MAMATHANG,SUM(SOLUONG) as SOLUONGDABAN from CT_DONDATHANG inner join DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
		where TINHTRANGDONHANG=2 and MAMATHANG like 'NC%' and month(NGAY)=@thang and year(NGAY)=@nam 
		group by MAMATHANG)
		)BANGTONGHOP
	group by BANGTONGHOP.MAMATHANG
	order by TONGDABAN desc
end


--DEM SO DON DAT HANG CHUA XAC NHAN TRONG NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetSoSanPhamBanOnlineNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetSoSanPhamBanOnlineNgay] @ngay smallint,@thang smallint, @nam smallint
as
begin
	select SUM(SOLUONG) as SOLUONGDABANONLINE from DONDATHANG inner join CT_DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANGDONHANG=2
end
--gte so san pham da ban tai quan theo ngay

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetSoSanPhamBanTaiQuanNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetSoSanPhamBanTaiQuanNgay] @ngay smallint,@thang smallint, @nam smallint
as
begin
	select SUM(SOLUONG) as SOHOPCOMDABANTAIQUAN from DONGOIMON inner join CT_DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANG=1
end

--Get bang xem hang san pham trong thang
--lay xep hang mon an da ban

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChi1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChi1Ngay] @ngay smalldatetime
as
begin
	select sum(TONGCHI) as TONGCHI from 
	(
		select sum(THANHTIEN)as TONGCHI from NHAPMATHANG
		where day(NGAYNHAP)=day(@ngay) and month(NGAYNHAP)=month(@ngay) and year(NGAYNHAP)=year(@ngay)
		union
		select sum(TONGLUONG) as TONGCHI from THANHTOANLUONGNHANVIEN 
		where day(NGAYTHANHTOAN)=day(@ngay) and month(NGAYTHANHTOAN)=month(@ngay) and year(NGAYTHANHTOAN)=year(@ngay)
		union
		select sum(SOTIEN) as TONGCHI from CHIPHIKHAC 
		where day(NGAYTRA)=day(@ngay) and month(NGAYTRA)=month(@ngay) and year(NGAYTRA)=year(@ngay) and ISCHIPHITHU=0
	)MIXTABLE
end

--Lay Tong thu 1 thang

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiKhac]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiKhac] @thang smallint, @nam smallint
as
begin
	select sum(SOTIEN) as TIENCHIKHAC from CHIPHIKHAC 
	where month(NGAYTRA)=@thang and year(NGAYTRA)=@nam and ISCHIPHITHU=0
end


--------------------------------
--BAOCAOBANHANG
--get so san pham da ban online theo ngay

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiKhacTrongNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiKhacTrongNgay] @ngay smallint, @thang smallint, @nam smallint
as
begin
	select sum(SOTIEN) as TIENCHIKHAC from CHIPHIKHAC 
	where day(NGAYTRA)=@ngay and  month(NGAYTRA)=@thang and year(NGAYTRA)=@nam and ISCHIPHITHU=0
end


--GETMAKHACHHANG LAST--

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiNhapDungCu]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiNhapDungCu] @thang smallint,@nam smallint
as
begin
	select sum(THANHTIEN)as TIENNHAPDC from NHAPMATHANG
	where (MAMATHANG like 'DC%') and month(NGAYNHAP)=@thang and year(NGAYNHAP)=@nam
end
--Nhap nuoc giai khat

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiNhapDungCuTrongNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiNhapDungCuTrongNgay] @ngay smallint, @thang smallint,@nam smallint
as
begin
	select sum(THANHTIEN)as TIENNHAPDC from NHAPMATHANG
	where (MAMATHANG like 'DC%') and day(NGAYNHAP)=@ngay and  month(NGAYNHAP)=@thang and year(NGAYNHAP)=@nam
end
--Nhap nuoc giai khat

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiNhapNguyenLieu]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiNhapNguyenLieu] @thang smallint,@nam smallint
as
begin
	select sum(THANHTIEN)as TIENNHAPNL from NHAPMATHANG
	where (MAMATHANG like 'NL%') and month(NGAYNHAP)=@thang and year(NGAYNHAP)=@nam
end
--nhap dung cu

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiNhapNguyenLieuTrongNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiNhapNguyenLieuTrongNgay] @ngay smallint, @thang smallint,@nam smallint
as
begin
	select sum(THANHTIEN)as TIENNHAPNL from NHAPMATHANG
	where (MAMATHANG like 'NL%') and day(NGAYNHAP)=@ngay and month(NGAYNHAP)=@thang and year(NGAYNHAP)=@nam
end
--nhap dung cu

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiNhapNuocGiaiKhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiNhapNuocGiaiKhat] @thang smallint,@nam smallint
as
begin
	select sum(THANHTIEN)as TIENNHAPNGK from NHAPMATHANG
	where (MAMATHANG like 'NC%') and month(NGAYNHAP)=@thang and year(NGAYNHAP)=@nam
end
--Tra luong nhan vien

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiNhapNuocGiaiKhatTrongNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiNhapNuocGiaiKhatTrongNgay] @ngay smallint, @thang smallint,@nam smallint
as
begin
	select sum(THANHTIEN)as TIENNHAPNGK from NHAPMATHANG
	where (MAMATHANG like 'NC%') and day(NGAYNHAP)=@ngay and  month(NGAYNHAP)=@thang and year(NGAYNHAP)=@nam
end
--Tra luong nhan vien

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiTraLuongNhanVien]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiTraLuongNhanVien] @thang smallint, @nam smallint
as
begin
	select sum(TONGLUONG) as TIENTRALUONGNHANVIEN from THANHTOANLUONGNHANVIEN 
	where month(NGAYTHANHTOAN)=@thang and year(NGAYTHANHTOAN)=@nam 
end
--Khac

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongChiTraLuongNhanVienTrongNgay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongChiTraLuongNhanVienTrongNgay] @ngay smallint, @thang smallint, @nam smallint
as
begin
	select sum(TONGLUONG) as TIENTRALUONGNHANVIEN from THANHTOANLUONGNHANVIEN 
	where day(NGAYTHANHTOAN)=@ngay and month(NGAYTHANHTOAN)=@thang and year(NGAYTHANHTOAN)=@nam 
end
--Khac

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThu1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThu1Ngay] @ngay smalldatetime
as
begin
	select sum(TONGTHU)as TONGTHU from(
		select sum(SOLUONG*DONGIA) as TONGTHU from DONGOIMON inner join CT_DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
		where day(NGAY)=day(@ngay) and month(NGAY)=month(@ngay) and year(NGAY)=year(@ngay) and TINHTRANG=1
		union
		select sum(SOLUONG*DONGIA) as TONGTHU from DONDATHANG inner join CT_DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
		where day(NGAY)=day(@ngay) and month(NGAY)=month(@ngay) and year(NGAY)=year(@ngay) and TINHTRANGDONHANG=2
		union
		select sum(SOTIEN) as TONGTHU from CHIPHIKHAC 
		where day(NGAYTRA)=day(@ngay) and month(NGAYTRA)=month(@ngay) and year(NGAYTRA)=year(@ngay) and ISCHIPHITHU=1)MIXTABLE
end
--LAY TONG CHI 1 NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuBanHangTaiQuan1Thang]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuBanHangTaiQuan1Thang] @thang smallint,@nam smallint
as
begin
	select sum(SOLUONG*DONGIA) as MUAHANGTAIQUAN from DONGOIMON inner join CT_DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
	where month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANG=1
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuBanHangTrenWeb1Thang]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuBanHangTrenWeb1Thang] @thang smallint,@nam smallint
as
begin
	select sum(SOLUONG*DONGIA) as MUAHANGQUAWEB from DONDATHANG inner join CT_DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
	where month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANGDONHANG=2
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuBuoiToi1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuBuoiToi1Ngay]  @ngay smallint,@thang smallint, @nam smallint
as
begin
	select sum(TONGTHU) from(
	select sum(SOLUONG*DONGIA) as TONGTHU from DONDATHANG inner join CT_DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANGDONHANG=2 and THOIGIAN>='15'
	union
	select sum(SOLUONG*DONGIA) as TONGTHU from DONGOIMON inner join CT_DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANG=1 and THOIGIAN>='15')A
end

--GET SO TIEN CHI TRONG NGAY
--nhap nguyen lieu

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuBuoiTrua1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuBuoiTrua1Ngay]  @ngay smallint,@thang smallint, @nam smallint
as
begin
	select sum(TONGTHU) from(
	select sum(SOLUONG*DONGIA) as TONGTHU from DONDATHANG inner join CT_DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANGDONHANG=2 and THOIGIAN<'15'
	union
	select sum(SOLUONG*DONGIA) as TONGTHU from DONGOIMON inner join CT_DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANG=1 and THOIGIAN<'15')A
end
--GET TONG THU Buoi Toi 1 NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuKhac]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuKhac] @thang smallint,@nam smallint
as
begin
	select sum(SOTIEN) as TIENTHUKHAC from CHIPHIKHAC 
	where month(NGAYTRA)=@thang and year(NGAYTRA)=@nam and ISCHIPHITHU=1
end

--Lay tong chi 1 thang
--nhap nguyen lieu

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuKhac1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuKhac1Ngay]  @ngay smallint,@thang smallint, @nam smallint
as
begin
		select sum(SOTIEN) as TONGTHU from CHIPHIKHAC 
		where day(NGAYTRA)=@ngay and month(NGAYTRA)=@thang and year(NGAYTRA)=@nam and ISCHIPHITHU=1
end



--GET TONG THU Buoi Trua 1 NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuOnline1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuOnline1Ngay]  @ngay smallint,@thang smallint, @nam smallint
as
begin
	select sum(SOLUONG*DONGIA) as TONGTHU from DONDATHANG inner join CT_DONDATHANG on DONDATHANG.MADON=CT_DONDATHANG.MADON
		where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANGDONHANG=2
end
--GET TONG THU KHAC 1 ngay

GO
/****** Object:  StoredProcedure [dbo].[Sp_BaoCao_GetTongThuTaiQuan1Ngay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_BaoCao_GetTongThuTaiQuan1Ngay]  @ngay smallint,@thang smallint, @nam smallint
as
begin
		select sum(SOLUONG*DONGIA) as TONGTHU from DONGOIMON inner join CT_DONGOIMON on DONGOIMON.MADON=CT_DONGOIMON.MADON
		where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam and TINHTRANG=1	
end
--GET TONG THU ONLINE 1 NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_CHAMCONGNHANVIEN_GetByMaNhanVien]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CHAMCONGNHANVIEN_GetByMaNhanVien] @maNhanVien varchar(10)
as
begin
	select* from CHAMCONGNHANVIEN where MANHANVIEN=@maNhanVien
	order by NGAYCHAMCONG desc
end

--THANHTOANLUONGNHANVIEN_GetByMaNhanVien

GO
/****** Object:  StoredProcedure [dbo].[Sp_CHAMCONGNHANVIEN_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CHAMCONGNHANVIEN_Insert] @maNhanVien varchar(10),@ngayCham smalldatetime,@thoiGianLam float, @trangThaiThanhToan bit
as
begin
	INSERT INTO CHAMCONGNHANVIEN(MANHANVIEN,NGAYCHAMCONG,THOIGIANLAM,TRANGTHAITHANHTOAN)
	values(@maNhanVien,@ngayCham,@thoiGianLam,@trangThaiThanhToan)
end


--INSERT DONGOIMON--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CHAMCONGNHANVIEN_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CHAMCONGNHANVIEN_Update] @maNhanVien varchar(10), @ngayChamCong smalldatetime, @thoiGianLam float 
as
begin
	UPDATE CHAMCONGNHANVIEN
	set THOIGIANLAM=@thoiGianLam 
	where MANHANVIEN=@maNhanVien and NGAYCHAMCONG=@ngayChamCong
end

--UPDATE All TRANG THAI CHAMCONGNHANVIEN

GO
/****** Object:  StoredProcedure [dbo].[Sp_CHAMCONGNHANVIEN_UpdateAllTrangThai]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CHAMCONGNHANVIEN_UpdateAllTrangThai] @maNhanVien varchar(10), @trangThaiThanhToan bit
as
begin
	UPDATE CHAMCONGNHANVIEN
	set TRANGTHAITHANHTOAN=@trangThaiThanhToan
	where MANHANVIEN=@maNhanVien
end

--UPDATE THANHTOANLUONGNHANVIEN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONDATHANG_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Sp_CT_DONDATHANG_Delete] @maCTDDH varchar(9)
as
begin
	DELETE from CT_DONDATHANG where MACTDDH=@maCTDDH
end

--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONDATHANG_GetByMADON]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_DONDATHANG_GetByMADON] @MaDon varchar(8)
as
begin
	select * from CT_DONDATHANG where MADON=@MaDon
end


----------------------------------------------------------------------------------------------------------------------
--LAY THONG TIN MAT HANG QUA MAMATHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONDATHANG_GetMaMoiNhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_DONDATHANG_GetMaMoiNhat]
as begin
select top 1 MACTDDH from CT_DONDATHANG
order by MACTDDH desc
end

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONDATHANG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[Sp_CT_DONDATHANG_Insert] 
@maCTDDH varchar(9),
@maDon varchar(8), 
@maMH varchar(5), 
@soLuong int
as
begin
	declare @giaBan int
	select @giaBan=sum(B.GIABAN) from 
		(select top 1 A.GIABAN from
			(select MAMONAN as MAMATHANG,GIABAN from MONAN UNION ALL select MANUOCGIAIKHAT as MAMATHANG, GIABAN from NUOCGIAIKHAT)A
			where MAMATHANG=@maMH)B
			
	insert into CT_DONDATHANG(MACTDDH,MADON,MAMATHANG,SOLUONG,DONGIA)
	values(@maCTDDH,@maDon,@maMH,@soLuong,@giaBan)
end

---------------------------------------------------------------------------------------------
--INSERT NHACUNGCAP--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONDATHANG_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_DONDATHANG_Update] 
@maCTDDH varchar(9), 
@maMH varchar(5), 
@soLuong int
as
begin
	declare @giaBan int
	select @giaBan=sum(B.GIABAN) from 
		(select top 1 A.GIABAN from
			(select MAMONAN as MAMATHANG,GIABAN from MONAN UNION ALL select MANUOCGIAIKHAT as MAMATHANG, GIABAN from NUOCGIAIKHAT)A
			where MAMATHANG=@maMH)B
			
	UPDATE CT_DONDATHANG
	SET
	MAMATHANG=@maMH,
	SOLUONG=@soLuong,
	DONGIA=@giaBan
	where MACTDDH=@maCTDDH
end


--UPDATE NHACUNGCAP--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONGOIMON_GetByMaDonGoiMon]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_DONGOIMON_GetByMaDonGoiMon] @MaDon varchar(8)
as
begin
select * from CT_DONGOIMON where MADON=@MaDon order by MACTDGM asc
end

--GET MA CT_DONGOIMON MOI NHAT

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONGOIMON_GetMaMoiNhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_DONGOIMON_GetMaMoiNhat]
as
begin
	select top 1 MACTDGM from CT_DONGOIMON order by MACTDGM desc
end


--DONGOIMON List3DaLamGanNhat

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_DONGOIMON_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_DONGOIMON_Insert] @maCT varchar(9), @maDon varchar(8), @maMatHang varchar(5), @soLuong int,@donGia int
as
begin
	insert into CT_DONGOIMON(MACTDGM,MADON,MAMATHANG,SOLUONG,DONGIA) 
	values (@maCT,@maDon,@maMatHang,@soLuong,@donGia)
end

--INSERT CT_GIOHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_GIOHANG_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_GIOHANG_Delete] @maKH varchar(10), @maMH varchar(5)
as
begin
	DELETE FROM CT_GIOHANG where MAMATHANG=@maMH and MAKHACHHANG=@maKH
end



--DELETE KHACHHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_GIOHANG_DemMatHangTrongGio]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_GIOHANG_DemMatHangTrongGio] @MaKH varchar(10), @MaMH varchar(5)
as
begin
	select count(MAMATHANG) from CT_GIOHANG where MAKHACHHANG=@MaKH and MAMATHANG=@MaMH
end

--DONDATHANG_ListByMaKH


GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_GIOHANG_GetAllByTaiKhoan]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_GIOHANG_GetAllByTaiKhoan] @taiKhoan varchar(50)
as
begin
	select * from CT_GIOHANG where MAKHACHHANG in 
	(select top 1 ID as MAKHACHHANG from ACCOUNT where TAIKHOAN=@taiKhoan)
end


--CT_GIOHANG DemMatHangTrongGio

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_GIOHANG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_GIOHANG_Insert] @maKH varchar(10), @maMH varchar(5),@soLuong int
as
begin
	insert into CT_GIOHANG(MAKHACHHANG,MAMATHANG,SOLUONG) values(@maKH,@maMH,@soLuong)
end


--INSERT DONDATHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_GIOHANG_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_GIOHANG_Update] @maKH varchar(10), @maMH varchar(5),@soLuong int
as
begin
	UPDATE CT_GIOHANG
	set SOLUONG=@soLuong
	where MAKHACHHANG=@maKH and MAMATHANG=@maMH
end

--HUY DONDATHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_MONAN_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_MONAN_Delete] @MaMonAn varchar(5)
as
begin
	delete from CT_MONAN where MAMONAN=@MaMonAn
end


--Delete MONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_MONAN_GetByMAMONAN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_MONAN_GetByMAMONAN] @MaMonAn varchar(5)
as
begin
	select * from CT_MONAN where MAMONAN=@MaMonAn
end

--GET NGUYENLIEU BY MANGUYENLIEU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_MONAN_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_MONAN_Insert] @maMonAn varchar(5), @maNguyenLieu varchar(5), @soLuong int
as
begin
	INSERT into CT_MONAN(MAMONAN,MANGUYENLIEU,SOLUONG) values (@maMonAn,@maNguyenLieu,@soLuong)
end

--INSERT DONVITINH--

GO
/****** Object:  StoredProcedure [dbo].[Sp_CT_MONAN_NGUYENLIEU_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CT_MONAN_NGUYENLIEU_Delete] @maMonAn varchar(5), @maNguyenLieu varchar(5)
as
begin
	delete from CT_MONAN where MAMONAN=@maMonAn and MANGUYENLIEU=@maNguyenLieu
end

--Delete DONVITINH

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_GetByDay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_GetByDay] @ngay smallint, @thang smallint, @nam smallint
as
begin
	select * from DONDATHANG 
	where day(NGAY)=@ngay and month(NGAY)=@thang and year(NGAY)=@nam
	order by TINHTRANGDONHANG ASC, THOIGIAN ASC
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_GetByMaDon]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_GetByMaDon] @MaDon varchar(8)
as 
begin
	select * from DONDATHANG where MADON=@MaDon
end
exec Sp_DONDATHANG_GetByMaDon @MaDon='DH000001'
----------------------------------------------------------------------------------------------------------------------
--Lay Khach hang thong qua don dat hang--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_Insert] @maDon varchar(8), @ngay smalldatetime,@thoiGian varchar(8),@maKH varchar(10),
	@diaChiGiaoHang nvarchar(150),@ghiChu nvarchar(150),@tinhTrangDonHang smallint,@ghiChuNguoiBan nvarchar(150)
as
begin
	INSERT INTO DONDATHANG(MADON,NGAY,THOIGIAN,MAKHACHHANG,DIACHIGIAOHANG,GHICHU,TINHTRANGDONHANG,GHICHUCUANGUOIBAN)
	values(@maDon,@ngay,@thoiGian,@maKH,@diaChiGiaoHang,@ghiChu,@tinhTrangDonHang,@ghiChuNguoiBan)
end



--DANG KY--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_ListAll]
as
begin
select * from DONDATHANG
order by NGAY DESC, TINHTRANGDONHANG ASC
end

--LAY DON DAT HANG THEO NGAY--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_ListByDay]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_ListByDay]
@Ngay int,
@Thang int,
@Nam int
as
begin
select * from DONDATHANG
where day(DONDATHANG.NGAY)=@Ngay and month(DONDATHANG.NGAY)=@Thang and year(DONDATHANG.NGAY)=@Nam
order by NGAY DESC, TINHTRANGDONHANG ASC
end

--LAY DON DAT HANG THEO MADON

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_ListByMaKH]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_ListByMaKH] @maKH varchar(10)
as
begin
	select * from DONDATHANG where MAKHACHHANG=@maKH order by TINHTRANGDONHANG ASC, NGAY DESC, THOIGIAN DESC
end	




---------------------BAOCAOTHONGKE-------------------

--LAY TONG THU 1 NGAY

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_Update_HuyDon]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_Update_HuyDon] @maDon varchar(8)
as
begin
	UPDATE DONDATHANG SET TINHTRANGDONHANG=3
	where MADON=@maDon and TINHTRANGDONHANG=0
end

--UPDATE ACCOUNT KHACHHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONDATHANG_Update_TinhTrang_GhiChuBan]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONDATHANG_Update_TinhTrang_GhiChuBan] 
@maDon varchar(8), 
@tinhTrang smallint, 
@ghiChuNguoiBan nvarchar(150)
as
begin
	UPDATE DONDATHANG
	SET
	TINHTRANGDONHANG=@tinhTrang,
	GHICHUCUANGUOIBAN=@ghiChuNguoiBan
	where MADON=@maDon
end

--select * from DONDATHANG go

--UPDATE CT DON HANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONGOIMON_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONGOIMON_Delete] @MaDon varchar(8)
as
begin
	begin
		delete from CT_DONGOIMON where MADON=@MaDon
	end
	begin
		delete from DONGOIMON where MADON=@MaDon
	end
end

--DELETE CT_GIOHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONGOIMON_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONGOIMON_Insert] @maDon varchar(8), @ngay smalldatetime,@thoiGian varchar(8),@ghiChu nvarchar(150),@ban varchar(4)
as
begin
	insert into DONGOIMON(MADON,NGAY,THOIGIAN,GHICHU,BAN,TINHTRANG)
	values(@maDon,@ngay,@thoiGian,@ghiChu,@ban,0)
end


--INSERT CT_DONGOIMON

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONGOIMON_List3DaLamGanNhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONGOIMON_List3DaLamGanNhat] @ngay smalldatetime
as
begin
	select top 3 * from DONGOIMON 
	where TINHTRANG=1 and NGAY=@ngay
	order by THOIGIAN DESC
end

--MENUMONAN_ListCon

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONGOIMON_ListDangDoi]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONGOIMON_ListDangDoi]
as
begin
	select*from DONGOIMON where TINHTRANG=0 
	order by NGAY ASC, THOIGIAN ASC
end


--GET CT_DONGOIMON BY MADONGOIMON

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONGOIMON_UpdateTrangThai]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONGOIMON_UpdateTrangThai] @MaDon varchar(8),@trangThai bit
as
begin
	UPDATE DONGOIMON
	set TINHTRANG=@trangThai
	where MADON=@MaDon
end


-- UPDATE CT_GIOHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_Delete] @maDVT varchar(5)
as
begin
	delete from DONVITINH where MADONVITINH=@maDVT
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_GetAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_GetAll]
as
begin
	select * from DONVITINH
end

-----------------------------------------------------------------------------------
--GET MAMATHANG_LAST--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_GetByMaDonViTinh]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_GetByMaDonViTinh] @MaDonViTinh varchar(5)
as
begin
	select * from DONVITINH where MADONVITINH=@MaDonViTinh
end

--GET ALL DONVITINH--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_GetMaMoiNhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_GetMaMoiNhat]
as
begin
	select top 1 MADONVITINH from DONVITINH order by MADONVITINH desc
end

--LISTALL DONVITINHLUONG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_Insert] @maDVT varchar(5), @tenDVT nvarchar(8)
as
begin
	INSERT into DONVITINH(MADONVITINH, TENDONVITINH) values (@maDVT,@tenDVT)
end

--INSERT MENUMONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_ListAll]
as
begin
	select * from DONVITINH
end

--DONVITINH GetMAMOINHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINH_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINH_Update] @maDVT varchar(5), @tenDVT nvarchar(8)
as
begin
	UPDATE DONVITINH
	set TENDONVITINH=@tenDVT
	where MADONVITINH=@maDVT
end


--UPDATE MENUMONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINHLUONG_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINHLUONG_Delete] @maDVT varchar(5)
as
begin
	delete from DONVITINHLUONG where MADONVITINHLUONG=@maDVT
end


--Delete NHANVIEN

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINHLUONG_GetByMaDonViTinhLuong]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINHLUONG_GetByMaDonViTinhLuong] @maDonViTinhLuong varchar(5)
as
begin
	select* from DONVITINHLUONG where MADONVITINHLUONG=@maDonViTinhLuong
end


--MENUMONAN_ListCon

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINHLUONG_GetMaMoiNhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINHLUONG_GetMaMoiNhat]
as
begin
	select top 1 MADONVITINHLUONG from DONVITINHLUONG order by MADONVITINHLUONG desc
end


--LISTALL THAMSO--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINHLUONG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINHLUONG_Insert] @maDVT varchar(5), @tenDVT nvarchar(8)
as
begin
	INSERT into DONVITINHLUONG(MADONVITINHLUONG, TENDONVITINHLUONG) values (@maDVT,@tenDVT)
end


--INSERT MENUMONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINHLUONG_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINHLUONG_ListAll]
as
begin
	select * from DONVITINHLUONG
end

--DONVITINHLUONG GetMAMOINHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DONVITINHLUONG_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DONVITINHLUONG_Update] @maDVT varchar(5), @tenDVT nvarchar(8)
as
begin
	UPDATE DONVITINHLUONG
	set TENDONVITINHLUONG=@tenDVT
	where MADONVITINHLUONG=@maDVT
end


--UPDATE THAM SO--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DUNGCU_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DUNGCU_Delete] @maMatHang varchar(5)
as
begin
	delete from DUNGCU where MADUNGCU=@maMatHang
end

--Delete NUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DungCu_GetByMaDungCu]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DungCu_GetByMaDungCu] @MaDungCu varchar(5)
as
begin
	select * from DUNGCU where MADUNGCU=@MaDungCu
end

--Get MonAn by MAMONAN

GO
/****** Object:  StoredProcedure [dbo].[Sp_DUNGCU_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DUNGCU_Insert] @maDungCu varchar(5)
as
begin
	insert into DUNGCU(MADUNGCU) values(@maDungCu)	
end

--INSERT NUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_DUNGCU_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_DUNGCU_ListAll]
as 
begin
	select * from DUNGCU	
end

--LIST ALL NUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_KHACHHANG_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_KHACHHANG_Delete] @maKhachHang varchar(10)
as
begin
	begin
		delete from CT_GIOHANG where MAKHACHHANG=@maKhachHang
	end
	begin
		delete from KHACHHANG where MAKHACHHANG=@maKhachHang
	end
	begin
		delete from ACCOUNT where ID=@maKhachHang
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_KHACHHANG_GetByMAKHACHHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_KHACHHANG_GetByMAKHACHHANG]
@MaKhachHang varchar(10)
as
begin
select* from KHACHHANG where MAKHACHHANG=@MaKhachHang
end

--Lay thong tin account thong qua ma khach hang

GO
/****** Object:  StoredProcedure [dbo].[Sp_KHACHHANG_GetMaKhachHangLast]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_KHACHHANG_GetMaKhachHangLast]
as
begin
	select top 1 ID from ACCOUNT where LOAIUSER='kh' order by ID desc
end


--GET DON DAT HANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_KHACHHANG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_KHACHHANG_Insert] @maKhachHang varchar(10), @taiKhoan varchar(50), @matKhau varchar(32), 
		@hoTen nvarchar(50), @ngaySinh smalldatetime, @gioiTinh nvarchar(3),
		@sdt varchar(11), @diaChi nvarchar(150)
as
begin
	begin
		insert into ACCOUNT(ID,TAIKHOAN,MATKHAU,HOTEN,NGAYSINH,GIOITINH,SDT,DIACHI,LOAIUSER) 
		values (@maKhachHang,@taiKhoan,@matKhau,@hoTen,@ngaySinh,@gioiTinh,@sdt,@diaChi,'kh')
	end
	begin
		insert into KHACHHANG(MAKHACHHANG,DIACHINHANHANG)
		values (@maKhachHang,N'')
	end
end





GO
/****** Object:  StoredProcedure [dbo].[Sp_KHACHHANG_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_KHACHHANG_Update] @maKH varchar(10), @diaChiNhanHang nvarchar(150)
as
begin
	UPDATE KHACHHANG 
	SET DIACHINHANHANG=@diaChiNhanHang
	where MAKHACHHANG=@maKH
end




GO
/****** Object:  StoredProcedure [dbo].[Sp_LOAIMATHANG_GetAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_LOAIMATHANG_GetAll]
as
begin
	select * from LOAIMATHANG
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_LOAIMATHANG_GetAllLoaiMatHangNhap]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_LOAIMATHANG_GetAllLoaiMatHangNhap]
as
begin
	select * from LOAIMATHANG where MALOAIMATHANG<> 'LH004'
end


--GET ALL MATHANG BY TYPE--

GO
/****** Object:  StoredProcedure [dbo].[Sp_LOAIMATHANG_GetByMALOAIMATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_LOAIMATHANG_GetByMALOAIMATHANG] @MaLoaiMatHang varchar(5)
as
begin
	select * from LOAIMATHANG where MALOAIMATHANG=@MaLoaiMatHang
end

--Get ALL Loai mat hang--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_Delete] @maMatHang varchar(5)
as
begin
	delete from MATHANG where MAMATHANG=@maMatHang
end

--Delete NHAPMATHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_GetAll_NuocGiaiKhat_MonAn]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_GetAll_NuocGiaiKhat_MonAn]
as
begin
select * from MATHANG where MALOAIMATHANG='LH003' or MALOAIMATHANG='LH004'
end


--------------------------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_GetAllByType]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_GetAllByType] @MaLoaiMatHang varchar(5)
as
begin
	select * from MATHANG where MALOAIMATHANG=@MaLoaiMatHang
end


--LIST ALL NGUYEN LIEU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_GetByMAMATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_GetByMAMATHANG] @MaMatHang varchar(5)
as
begin
	select * from MATHANG where MAMATHANG=@MaMatHang
end

--Get Nguyen lieu by MaNguyenLieu

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_GetMaMatHangLast]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_GetMaMatHangLast] @maLoaiMatHang varchar(5)
as
begin
	select top 1 MAMATHANG from MATHANG
	where MALOAIMATHANG=@maLoaiMatHang
	order by MAMATHANG DESC
end

select * from MATHANG


------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_GetNhaCungCap]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_GetNhaCungCap] @MaMatHang varchar(5)
as
begin
	select * from NHACUNGCAP where MANHACUNGCAP in 
	(
		select MANHACUNGCAP from MATHANGNHAP_NHACUNGCAP where MAMATHANG=@MaMatHang
	)
end


--------------------------------------------------------------------------------------------------------------------
--Get Loai mat hang--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_Insert] 
@maMatHang varchar(5),
@tenMatHang nvarchar(30), 
@maDonViTinh varchar(5),
@maLoaiMatHang varchar(5)
as
begin
	insert into MATHANG(MAMATHANG,TENMATHANG,MADONVITINH,MALOAIMATHANG) 
	values(@maMatHang,@tenMatHang,@maDonViTinh,@maLoaiMatHang)
end

--INSERT NGUYENLIEU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANG_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANG_Update] 
@maMatHang varchar(5), @tenMatHang nvarchar(30), @maDonVi varchar(5)
as
begin
	UPDATE MATHANG
	set
	TENMATHANG=@tenMatHang, MADONVITINH=@maDonVi
	where MAMATHANG=@maMatHang
end

--UPDATE NHAPMATHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANGNHAP_NHACUNGCAP_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANGNHAP_NHACUNGCAP_Delete]  
@maMatHang varchar(5),
@maNhaCungCap varchar(7)
as
begin
	delete from MATHANGNHAP_NHACUNGCAP where MAMATHANG=@maMatHang and MANHACUNGCAP=@maNhaCungCap
end

--Delete NGUYENLIEU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MATHANGNHAP_NHACUNGCAP_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MATHANGNHAP_NHACUNGCAP_Insert]  
@maMatHang varchar(5),
@maNhaCungCap varchar(7)
as
begin
	insert into MATHANGNHAP_NHACUNGCAP(MAMATHANG,MANHACUNGCAP) values(@maMatHang,@maNhaCungCap)
end

--INSERT MATHANG

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_Delete] @MaMonAn varchar(5)
as
begin
	delete from MENUMONAN where MAMONAN=@MaMonAn
end

--Delete CT_MONAN

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_Insert] @maMonAn varchar(5), @trangThai bit, @giaBan int
as
begin
	insert into MENUMONAN(MAMONAN,TRANGTHAI) values (@maMonAn,@trangThai)
	update MONAN
	set GIABAN=@giaBan where MAMONAN=@maMonAn
end

--INSERT MENUNUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_ListAll]
as
begin
	select* from MENUMONAN
end

--LIST ALL MENUNUOCGIAIKHAT

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_ListCon]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_ListCon]
as
begin
	select top 8 * from MENUMONAN where TRANGTHAI=1
end
--MENUNUOCGIAIKHAT_ListCon

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_Update] @maMonAn varchar(5), @trangThai bit, @giaBan int
as
begin
	UPDATE MENUMONAN
	set TRANGTHAI=@trangThai where MAMONAN=@maMonAn
	update MONAN
	set GIABAN=@giaBan where MAMONAN=@maMonAn
end

--UPDATE MENUMONAN-ALL

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_UpdateAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_UpdateAll] @trangThai bit
as
begin
	UPDATE MENUMONAN
	set TRANGTHAI=@trangThai
end

--UPDATE MENUNUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUMONAN_UpdateTrangThai]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUMONAN_UpdateTrangThai] @maMonAn varchar(5), @trangThai bit
as
begin
	UPDATE MENUMONAN
	set TRANGTHAI=@trangThai where MAMONAN=@maMonAn
end

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUNUOCGIAIKHAT_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUNUOCGIAIKHAT_Delete] @maMatHang varchar(5)
as
begin
	delete from MENUNUOCGIAIKHAT where MANUOCGIAIKHAT=@maMatHang
end

--Delete MenuMonAn--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUNUOCGIAIKHAT_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUNUOCGIAIKHAT_Insert] @maNuocGiaiKhat varchar(5), @giaBan int
as
begin
	INSERT into MENUNUOCGIAIKHAT(MANUOCGIAIKHAT) values(@maNuocGiaiKhat)
	UPDATE NUOCGIAIKHAT
	set GIABAN=@giaBan
	where MANUOCGIAIKHAT=@maNuocGiaiKhat
end

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUNUOCGIAIKHAT_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUNUOCGIAIKHAT_ListAll]
as
begin
	select * from MENUNUOCGIAIKHAT
end

------------------------------------------------------------------------------------------------------------------
--GET DONVITINHLUONG by MaDonViTinhLuong--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUNUOCGIAIKHAT_ListCon]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUNUOCGIAIKHAT_ListCon]
as
begin
	select top 4 * from MENUNUOCGIAIKHAT where MANUOCGIAIKHAT in 
	(select MANUOCGIAIKHAT from NUOCGIAIKHAT where SOLUONGCON>0)
end


GO
/****** Object:  StoredProcedure [dbo].[Sp_MENUNUOCGIAIKHAT_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MENUNUOCGIAIKHAT_Update] @maNuocGiaiKhat varchar(5), @soLuongCon int, @giaBan int
as
begin
	update NUOCGIAIKHAT
	set GIABAN=@giaBan, SOLUONGCON=@soLuongCon where MANUOCGIAIKHAT=@maNuocGiaiKhat
end



--UPDATE

GO
/****** Object:  StoredProcedure [dbo].[Sp_MONAN_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MONAN_Delete] @maMatHang varchar(5)
as
begin
	delete from MONAN where MAMONAN=@maMatHang
end

--DELETE CT_MONAN

GO
/****** Object:  StoredProcedure [dbo].[Sp_MONAN_GetByMaMonAn]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MONAN_GetByMaMonAn] @MaMonAn varchar(5)
as
begin
	select * from MONAN where MAMONAN=@MaMonAn
end


----------------------------------------------------------------------------------------------------------------------
select * from LOAIMATHANG

--LAY MATHANG NUOC GIAI KHAT VS MON AN

GO
/****** Object:  StoredProcedure [dbo].[Sp_MONAN_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MONAN_Insert] @maMonAn varchar(5) , @giaban int , @hinhAnh varchar(150)
as
begin
	INSERT INTO MONAN(MAMONAN,GIABAN,HINHANH) values(@maMonAn, @giaban,@hinhAnh)
end

--LISTALL DONVITINH--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MONAN_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MONAN_ListAll]
as
begin
	select * from MONAN
end

--GET CT_MONAN BY MAMONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_MONAN_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_MONAN_Update] @maMonAn varchar(5), @giaBan int, @hinhAnh varchar(150)
as
begin
	UPDATE MONAN 
	set
	GIABAN=@giaBan, HINHANH=@hinhAnh
	where MAMONAN=@maMonAn
end

--UPDATE DONVITINH--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NGUYENLIEU_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NGUYENLIEU_Delete] @maMatHang varchar(5)
as
begin
	delete from NGUYENLIEU where MANGUYENLIEU=@maMatHang
end

--Delete MATHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NGUYENLIEU_GetByMaNguyenLieu]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NGUYENLIEU_GetByMaNguyenLieu] @MaNguyenLieu varchar(5)
as
begin
	select * from NGUYENLIEU where MANGUYENLIEU=@MaNguyenLieu
end

--Get Nuoc giai khat by MaNuocGiaiKhat

GO
/****** Object:  StoredProcedure [dbo].[Sp_NGUYENLIEU_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NGUYENLIEU_Insert] @maNguyenLieu varchar(5)
as
begin
	insert into NGUYENLIEU(MANGUYENLIEU) values(@maNguyenLieu)	
end


--INSERT NHAPMATHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NGUYENLIEU_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NGUYENLIEU_ListAll]
as 
begin
	select * from NGUYENLIEU	
end

--GET DONVITINH TU MADONVITINH--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_Delete] @maNhaCungCap varchar(7)
as
begin
	delete from MATHANGNHAP_NHACUNGCAP where MANHACUNGCAP=@maNhaCungCap
	begin
	delete from NHACUNGCAP where MANHACUNGCAP=@maNhaCungCap 
	end
end

--detele MATHANGNHAP_NHACUNGCAP

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_GetAllByType]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_GetAllByType] @MaLoaiMatHang varchar(5)
as
begin
	select* from NHACUNGCAP where MALOAIMATHANGCUNGCAP=@MaLoaiMatHang
end

--LIST NHACUNGCAP by MAMATHANG--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_GetByMaNhaCungCap]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_GetByMaNhaCungCap] @MaNhaCungCap varchar(7)
as
begin
	select * from NHACUNGCAP where MANHACUNGCAP=@MaNhaCungCap
end
--GET NHACUNGCAP BY TYPE--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_GetMaMoiNhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_GetMaMoiNhat]
as begin
select top 1 MANHACUNGCAP from NHACUNGCAP
order by MANHACUNGCAP desc
end

--LIST ALL NHA CUNG CAP--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_Insert]
@maNCC varchar(7), @tenNCC nvarchar(50),@nguoiDaiDien nvarchar(50),@sdt varchar(11),@diaChi nvarchar(150),@maLoaiMHCC varchar(5),@moTa nvarchar(150)
as
begin
	insert into NHACUNGCAP(MANHACUNGCAP,TENNHACUNGCAP,NGUOIDAIDIEN,SODIENTHOAI,DIACHI,MOTA,MALOAIMATHANGCUNGCAP)
	values (@maNCC,@tenNCC,@nguoiDaiDien,@sdt,@diaChi,@moTa,@maLoaiMHCC)
end



--INSERT MATHANGNHAP_NHACUNGCAP--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_ListAll]
as begin
	select * from NHACUNGCAP
end

--GET NHACUNGCAP BY MA--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHACUNGCAP_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHACUNGCAP_Update]
@maNCC varchar(7), @tenNCC nvarchar(50),@nguoiDaiDien nvarchar(50),@sdt varchar(11),@diaChi nvarchar(150),@maLoaiMHCC varchar(5),@moTa nvarchar(150)
as
begin
	UPDATE NHACUNGCAP
	set TENNHACUNGCAP=@tenNCC, NGUOIDAIDIEN=@nguoiDaiDien, SODIENTHOAI=@sdt, DIACHI=@diaChi, MALOAIMATHANGCUNGCAP=@maLoaiMHCC,MOTA=@moTa
	where MANHACUNGCAP=@maNCC
end



--UPDATE NGUYEN LIEU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_Delete] @maNhanVien varchar(10)
as
begin
	begin
		delete from CHAMCONGNHANVIEN where MANHANVIEN=@maNhanVien
		delete from THANHTOANLUONGNHANVIEN where MANHANVIEN=@maNhanVien
	end
	begin
		delete from NHANVIEN where MANHANVIEN=@maNhanVien
	end
	begin
		delete from ACCOUNT where ID=@maNhanVien
	end
end

--delete THANHTOANLUONGNHANVIEN

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_GetByMaNhanVien]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_GetByMaNhanVien] @maNhanVien varchar(10)
as
begin
	select * from NHANVIEN where MANHANVIEN=@maNhanVien
end

--TINHTONG TIEN LUONG CHUA THANH TOAN

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_GetMaNhanVienLast]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_GetMaNhanVienLast]
as
begin
	select top 1 ID from ACCOUNT where LOAIUSER='nv' order by ID desc
end

--GET NHANVIEN BY MANHANVIEN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_Insert] @maNhanVien varchar(10), @taiKhoan varchar(50), @matKhau varchar(32), 
		@hoTen nvarchar(50), @ngaySinh smalldatetime, @gioiTinh nvarchar(3),
		@sdt varchar(11), @diaChi nvarchar(150), @luong int , @maDVL varchar(5), @ngayKyHopDong smalldatetime
as
begin
	set dateformat dmy
	begin
		insert into ACCOUNT(ID,TAIKHOAN,MATKHAU,HOTEN,NGAYSINH,GIOITINH,SDT,DIACHI,LOAIUSER) 
		values (@maNhanVien,@taiKhoan,@matKhau,@hoTen,@ngaySinh,@gioiTinh,@sdt,@diaChi,'nv')
	end
	begin
		insert into NHANVIEN(MANHANVIEN,LUONG,MADONVITINHLUONG,NGAYKYHOPDONG,TRANGTHAI)
		values (@maNhanVien,@luong,@maDVL,@ngayKyHopDong,1)
	end
end

--INSERT THANH TOAN LUONG

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_ListAll]
as
begin
	select * from NHANVIEN
	order by TRANGTHAI asc
end


--GET DONVITINHLUONG by MaDonViTinhLuong--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_ListAllConLamViec]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_ListAllConLamViec]
as
begin
	select* from NHANVIEN where TRANGTHAI=1
end

--NHANVIEN LISTALL Nghi Viec--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_ListAllNghiViec]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_ListAllNghiViec]
as
begin
	select* from NHANVIEN where TRANGTHAI=0
end

--LAY CAC DONGOIMON DANG DOI THEO THU TU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_TinhTongSoTienLuongChuaThanhToan]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_TinhTongSoTienLuongChuaThanhToan] @maNhanVien varchar(10)
as
begin
	select SUM(LUONG*THOIGIANLAM) as TONGTIEN from NHANVIEN inner join CHAMCONGNHANVIEN on NHANVIEN.MANHANVIEN=CHAMCONGNHANVIEN.MANHANVIEN
	where NHANVIEN.MANHANVIEN=@maNhanVien and TRANGTHAITHANHTOAN=0
end


--NHANVIEN LISTALL CONLAMVIEC--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHANVIEN_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHANVIEN_Update] @maNhanVien varchar(10), 
		@hoTen nvarchar(50), @ngaySinh smalldatetime, @gioiTinh nvarchar(3),
		@sdt varchar(11), @diaChi nvarchar(150), @luong int , @ngayKyHopDong smalldatetime,@maDVL varchar(5)
as
begin

	begin
		UPDATE ACCOUNT
		set
		HOTEN=@hoTen, NGAYSINH=@ngaySinh,GIOITINH=@gioiTinh,SDT=@sdt,DIACHI=@diaChi
		where ID=@maNhanVien
	end
	begin
		UPDATE NHANVIEN
		set
		LUONG=@luong,MADONVITINHLUONG=@maDVL,NGAYKYHOPDONG=@ngayKyHopDong
		where MANHANVIEN=@maNhanVien
	end
end

--UPDATE TK NHANVIEN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHAPMAHANG_GetMaPhieuNhapLast]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHAPMAHANG_GetMaPhieuNhapLast]
as
begin
	select top 1 MAPHIEUNHAP from NHAPMATHANG 
	order by MAPHIEUNHAP desc
end

-------------------------------------------------------------------------
--LIST ALL DUNGCU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHAPMATHANG_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHAPMATHANG_Delete] @MaPhieuNhap varchar(10)
as 
begin
	delete from NHAPMATHANG where MAPHIEUNHAP=@MaPhieuNhap
end

--Detete DUNGCU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHAPMATHANG_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHAPMATHANG_Insert] 
@MaPhieuNhap varchar(10), 
@MaMatHang varchar(5),
@MaNhaCungCap varchar(7),
@NgayNhap smalldatetime,
@SoLuong int,
@DonGia int,
@ThanhTien int,
@HanSuDung smalldatetime
as
begin
	set dateformat dmy
	if(@HanSuDung='01/01/1900')
		begin
		insert into NHAPMATHANG(MAPHIEUNHAP,MAMATHANG,MANHACUNGCAP,NGAYNHAP,SOLUONG,DONGIA,THANHTIEN,HANSUDUNG)
		values (@MaPhieuNhap,@MaMatHang,@MaNhaCungCap,@NgayNhap,@SoLuong,@DonGia,@ThanhTien,null)
		end
	else
		begin
		insert into NHAPMATHANG(MAPHIEUNHAP,MAMATHANG,MANHACUNGCAP,NGAYNHAP,SOLUONG,DONGIA,THANHTIEN,HANSUDUNG)
		values (@MaPhieuNhap,@MaMatHang,@MaNhaCungCap,@NgayNhap,@SoLuong,@DonGia,@ThanhTien,@HanSuDung)
		end
end

-------------------------------------------------------------------------------------
--INSERT DUNGCU--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHAPMATHANG_ListByDayAndType]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHAPMATHANG_ListByDayAndType] @ngayNhap smallint, @thangNhap smallint, @namNhap smallint, @maLoaiMatHang varchar(5)
as
begin
	select * from NHAPMATHANG 
	where day(NGAYNHAP)=@ngayNhap and month(NGAYNHAP)=@thangNhap and year(NGAYNHAP)=@namNhap
	and MAMATHANG in (select MAMATHANG from MATHANG where MALOAIMATHANG=@maLoaiMatHang)
end

--NHAPMATHANG GetMaPhieuNhapLast

GO
/****** Object:  StoredProcedure [dbo].[Sp_NHAPMATHANG_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NHAPMATHANG_Update] 
@MaPhieuNhap varchar(10), 
@MaMatHang varchar(5),
@MaNhaCungCap varchar(7),
@NgayNhap smalldatetime,
@SoLuong int,
@DonGia int,
@ThanhTien int,
@HanSuDung smalldatetime
as
begin
	set dateformat dmy
	if(@HanSuDung='01/01/1900')
		begin
		UPDATE NHAPMATHANG 
		set
		MAMATHANG=@MaMatHang, MANHACUNGCAP=@MaNhaCungCap, NGAYNHAP=@NgayNhap, SOLUONG=@SoLuong,DONGIA=@DonGia,
		THANHTIEN=@ThanhTien, HANSUDUNG=null
		where MAPHIEUNHAP=@MaPhieuNhap
		end
	else
		begin
		UPDATE NHAPMATHANG 
		set
		MAMATHANG=@MaMatHang, MANHACUNGCAP=@MaNhaCungCap, NGAYNHAP=@NgayNhap, SOLUONG=@SoLuong,DONGIA=@DonGia,
		THANHTIEN=@ThanhTien, HANSUDUNG=@HanSuDung
		where MAPHIEUNHAP=@MaPhieuNhap
		end
end

--UPDATE NUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NUOCGIAIKHAT_AddCount]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NUOCGIAIKHAT_AddCount] @maNuocGiaiKhat varchar(5), @soLuongThem int
as
begin
	declare @soLuongCon int
	UPDATE NUOCGIAIKHAT
	set SOLUONGCON=SOLUONGCON+@soLuongThem
	where MANUOCGIAIKHAT=@maNuocGiaiKhat

	select @soLuongCon=SUM(SOLUONGCON) from NUOCGIAIKHAT where MANUOCGIAIKHAT=@maNuocGiaiKhat
	if @soLuongCon<0
		begin
		UPDATE NUOCGIAIKHAT
		set SOLUONGCON=0
		where MANUOCGIAIKHAT=@maNuocGiaiKhat
		end

end


--UPDATE MONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NUOCGIAIKHAT_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NUOCGIAIKHAT_Delete] @maMatHang varchar(5)
as
begin
	begin
		delete from MENUNUOCGIAIKHAT where MANUOCGIAIKHAT=@maMatHang
	end

	begin
		delete from NUOCGIAIKHAT where MANUOCGIAIKHAT=@maMatHang
	end
end

--Delete MENUNUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NUOCGIAIKHAT_GetByMaNuocGiaiKhat]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NUOCGIAIKHAT_GetByMaNuocGiaiKhat] @MaNuocGiaiKhat varchar(5)
as
begin
	select * from NUOCGIAIKHAT where MANUOCGIAIKHAT=@MaNuocGiaiKhat
end

--Get Dung cu by MADUNGCU

GO
/****** Object:  StoredProcedure [dbo].[Sp_NUOCGIAIKHAT_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NUOCGIAIKHAT_Insert] @maNuocGiaiKhat varchar(5), @giaban int , @soLuongCon int, @hinhAnh varchar(150)
as
begin
	insert into NUOCGIAIKHAT(MANUOCGIAIKHAT,GIABAN,SOLUONGCON,HINHANH) values(@maNuocGiaiKhat,@giaban,@soLuongCon,@hinhAnh)	
end


--INSERT CT_MONAN-

GO
/****** Object:  StoredProcedure [dbo].[Sp_NUOCGIAIKHAT_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NUOCGIAIKHAT_ListAll]
as 
begin
	select * from NUOCGIAIKHAT	
end

--LIST ALL MONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_NUOCGIAIKHAT_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_NUOCGIAIKHAT_Update] @maNuocGiaiKhat varchar(5), @giaBan int, @soLuongCon int , @hinhAnh varchar(150)
as
begin
	UPDATE NUOCGIAIKHAT 
	set
	GIABAN=@giaBan, SOLUONGCON=@soLuongCon, HINHANH=@hinhAnh
	where MANUOCGIAIKHAT=@maNuocGiaiKhat
end

--Add them so luong NUOCGIAIKHAT--

GO
/****** Object:  StoredProcedure [dbo].[Sp_THAMSO_ListAll]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_THAMSO_ListAll]
as
begin
	select * from THAMSO
end

--LISTALL MENUMONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_THAMSO_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_THAMSO_Update] @maThamSo varchar(5), @giaTri int ,@tinhTrang bit
as
begin
	UPDATE THAMSO
	set
	GIATRI=@giaTri, TINHTRANG=@tinhTrang
	where MATHAMSO=@maThamSo
end


--UPDATE MENUMONAN--

GO
/****** Object:  StoredProcedure [dbo].[Sp_THANHTOANLUONGNHANVIEN_Delete]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_THANHTOANLUONGNHANVIEN_Delete] @maNhanVien varchar(10), @ngayThanhToan smalldatetime
as
begin
	delete from THANHTOANLUONGNHANVIEN
	where MANHANVIEN=@maNhanVien and NGAYTHANHTOAN=@ngayThanhToan
end


--delete DONGOIMON

GO
/****** Object:  StoredProcedure [dbo].[Sp_THANHTOANLUONGNHANVIEN_GetByMaNhanVien]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_THANHTOANLUONGNHANVIEN_GetByMaNhanVien] @maNhanVien varchar(10)
as
begin
	select * from CHAMCONGNHANVIEN where MANHANVIEN=@maNhanVien
	order by TRANGTHAITHANHTOAN asc, NGAYCHAMCONG desc
end	

--GETMANHANVIEN LAST--

GO
/****** Object:  StoredProcedure [dbo].[Sp_THANHTOANLUONGNHANVIEN_Insert]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_THANHTOANLUONGNHANVIEN_Insert] @maNhanVien varchar(10), @ngayThanhToan smalldatetime, @tongThoiGianLam float ,@tongLuong int,@khoangKhac int,@ghiChu nvarchar(150)
as
begin
	INSERT into THANHTOANLUONGNHANVIEN(MANHANVIEN,NGAYTHANHTOAN,TONGTHOIGIANLAM,TONGLUONG,KHOANGKHAC,GHICHU)
	values(@maNhanVien,@ngayThanhToan,@tongThoiGianLam,@tongLuong,@khoangKhac,@ghiChu)
end

--INSERT CHAMCONG

GO
/****** Object:  StoredProcedure [dbo].[Sp_THANHTOANLUONGNHANVIEN_Update]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_THANHTOANLUONGNHANVIEN_Update] @maNhanVien varchar(10), @ngayThanhToan smalldatetime,@khoangKhac int,@ghiChu nvarchar(150)
as
begin
	UPDATE THANHTOANLUONGNHANVIEN
	set KHOANGKHAC=@khoangKhac, GHICHU=@ghiChu
	where MANHANVIEN=@maNhanVien and NGAYTHANHTOAN=@ngayThanhToan
end


--UPDATE

GO
/****** Object:  Table [dbo].[ACCOUNT]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACCOUNT](
	[ID] [varchar](10) NOT NULL,
	[TAIKHOAN] [varchar](50) NOT NULL,
	[MATKHAU] [varchar](32) NOT NULL,
	[HOTEN] [nvarchar](50) NOT NULL,
	[NGAYSINH] [smalldatetime] NULL,
	[GIOITINH] [nvarchar](3) NULL,
	[SDT] [varchar](11) NULL,
	[DIACHI] [nvarchar](150) NULL,
	[LOAIUSER] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [TAIKHOAN_DuyNhat] UNIQUE NONCLUSTERED 
(
	[TAIKHOAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ADMIN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ADMIN](
	[MAADMIN] [varchar](10) NOT NULL,
	[QUYEN] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAADMIN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHAMCONGNHANVIEN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHAMCONGNHANVIEN](
	[MANHANVIEN] [varchar](10) NOT NULL,
	[NGAYCHAMCONG] [smalldatetime] NOT NULL,
	[THOIGIANLAM] [float] NULL,
	[TRANGTHAITHANHTOAN] [bit] NULL,
 CONSTRAINT [PK_CHAMCONGNHANVIEN] PRIMARY KEY CLUSTERED 
(
	[MANHANVIEN] ASC,
	[NGAYCHAMCONG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHIPHIKHAC]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHIPHIKHAC](
	[MACHIPHI] [varchar](8) NOT NULL,
	[TENCHIPHI] [nvarchar](50) NULL,
	[SOTIEN] [int] NULL,
	[NGAYTRA] [smalldatetime] NULL,
	[MOTA] [nvarchar](150) NULL,
	[ISCHIPHITHU] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACHIPHI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CT_DONDATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CT_DONDATHANG](
	[MACTDDH] [varchar](9) NOT NULL,
	[MADON] [varchar](8) NOT NULL,
	[MAMATHANG] [varchar](5) NOT NULL,
	[SOLUONG] [int] NULL,
	[DONGIA] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACTDDH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CT_DONGOIMON]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CT_DONGOIMON](
	[MACTDGM] [varchar](9) NOT NULL,
	[MADON] [varchar](8) NOT NULL,
	[MAMATHANG] [varchar](5) NOT NULL,
	[SOLUONG] [int] NULL,
	[DONGIA] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MACTDGM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CT_GIOHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CT_GIOHANG](
	[MAKHACHHANG] [varchar](10) NOT NULL,
	[MAMATHANG] [varchar](5) NOT NULL,
	[SOLUONG] [int] NULL,
 CONSTRAINT [PK_CT_GIOHANG] PRIMARY KEY CLUSTERED 
(
	[MAKHACHHANG] ASC,
	[MAMATHANG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CT_MONAN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CT_MONAN](
	[MAMONAN] [varchar](5) NOT NULL,
	[MANGUYENLIEU] [varchar](5) NOT NULL,
	[SOLUONG] [int] NULL,
 CONSTRAINT [PK_CT_MONAN] PRIMARY KEY CLUSTERED 
(
	[MAMONAN] ASC,
	[MANGUYENLIEU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DONDATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DONDATHANG](
	[MADON] [varchar](8) NOT NULL,
	[NGAY] [smalldatetime] NULL,
	[THOIGIAN] [varchar](8) NULL,
	[MAKHACHHANG] [varchar](10) NOT NULL,
	[DIACHIGIAOHANG] [nvarchar](150) NULL,
	[GHICHU] [nvarchar](150) NULL,
	[TINHTRANGDONHANG] [smallint] NULL,
	[GHICHUCUANGUOIBAN] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[MADON] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DONGOIMON]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DONGOIMON](
	[MADON] [varchar](8) NOT NULL,
	[NGAY] [smalldatetime] NULL,
	[THOIGIAN] [varchar](8) NULL,
	[BAN] [varchar](4) NULL,
	[GHICHU] [nvarchar](150) NULL,
	[TINHTRANG] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MADON] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DONVITINH]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DONVITINH](
	[MADONVITINH] [varchar](5) NOT NULL,
	[TENDONVITINH] [nvarchar](8) NULL,
PRIMARY KEY CLUSTERED 
(
	[MADONVITINH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DONVITINHLUONG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DONVITINHLUONG](
	[MADONVITINHLUONG] [varchar](5) NOT NULL,
	[TENDONVITINHLUONG] [nvarchar](6) NULL,
PRIMARY KEY CLUSTERED 
(
	[MADONVITINHLUONG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DUNGCU]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DUNGCU](
	[MADUNGCU] [varchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MADUNGCU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KHACHHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KHACHHANG](
	[MAKHACHHANG] [varchar](10) NOT NULL,
	[DIACHINHANHANG] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAKHACHHANG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOAIMATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOAIMATHANG](
	[MALOAIMATHANG] [varchar](5) NOT NULL,
	[TENLOAIMATHANG] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[MALOAIMATHANG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MATHANG](
	[MAMATHANG] [varchar](5) NOT NULL,
	[TENMATHANG] [nvarchar](30) NULL,
	[MADONVITINH] [varchar](5) NULL,
	[MALOAIMATHANG] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAMATHANG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MATHANGNHAP_NHACUNGCAP]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MATHANGNHAP_NHACUNGCAP](
	[MAMATHANG] [varchar](5) NOT NULL,
	[MANHACUNGCAP] [varchar](7) NOT NULL,
 CONSTRAINT [PK_MATHANGNHAP_NHACUNGCAP] PRIMARY KEY CLUSTERED 
(
	[MAMATHANG] ASC,
	[MANHACUNGCAP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MENUMONAN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MENUMONAN](
	[MAMONAN] [varchar](5) NOT NULL,
	[TRANGTHAI] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAMONAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MENUNUOCGIAIKHAT]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MENUNUOCGIAIKHAT](
	[MANUOCGIAIKHAT] [varchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MANUOCGIAIKHAT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MONAN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MONAN](
	[MAMONAN] [varchar](5) NOT NULL,
	[GIABAN] [int] NULL,
	[HINHANH] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAMONAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NGUYENLIEU]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NGUYENLIEU](
	[MANGUYENLIEU] [varchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MANGUYENLIEU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHACUNGCAP]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHACUNGCAP](
	[MANHACUNGCAP] [varchar](7) NOT NULL,
	[TENNHACUNGCAP] [nvarchar](50) NULL,
	[NGUOIDAIDIEN] [nvarchar](50) NULL,
	[SODIENTHOAI] [varchar](11) NULL,
	[DIACHI] [nvarchar](150) NULL,
	[MOTA] [nvarchar](150) NULL,
	[MALOAIMATHANGCUNGCAP] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANHACUNGCAP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[MANHANVIEN] [varchar](10) NOT NULL,
	[LUONG] [int] NULL,
	[MADONVITINHLUONG] [varchar](5) NULL,
	[NGAYKYHOPDONG] [smalldatetime] NULL,
	[TRANGTHAI] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MANHANVIEN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHAPMATHANG]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHAPMATHANG](
	[MAPHIEUNHAP] [varchar](10) NOT NULL,
	[MAMATHANG] [varchar](5) NOT NULL,
	[MANHACUNGCAP] [varchar](7) NOT NULL,
	[NGAYNHAP] [smalldatetime] NULL,
	[SOLUONG] [int] NULL,
	[DONGIA] [int] NULL,
	[THANHTIEN] [int] NULL,
	[HANSUDUNG] [smalldatetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MAPHIEUNHAP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NUOCGIAIKHAT]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NUOCGIAIKHAT](
	[MANUOCGIAIKHAT] [varchar](5) NOT NULL,
	[GIABAN] [int] NULL,
	[SOLUONGCON] [int] NULL,
	[HINHANH] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[MANUOCGIAIKHAT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[THAMSO]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THAMSO](
	[MATHAMSO] [varchar](5) NOT NULL,
	[TENTHAMSO] [nvarchar](50) NULL,
	[KIEU] [varchar](10) NULL,
	[GIATRI] [int] NULL,
	[TINHTRANG] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MATHAMSO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[THANHTOANLUONGNHANVIEN]    Script Date: 6/6/2020 7:11:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THANHTOANLUONGNHANVIEN](
	[MANHANVIEN] [varchar](10) NOT NULL,
	[NGAYTHANHTOAN] [smalldatetime] NOT NULL,
	[TONGTHOIGIANLAM] [float] NULL,
	[KHOANGKHAC] [int] NULL,
	[GHICHU] [nvarchar](150) NULL,
	[TONGLUONG] [int] NULL,
 CONSTRAINT [PK_THANHTOANLUONGNHANVIEN] PRIMARY KEY CLUSTERED 
(
	[MANHANVIEN] ASC,
	[NGAYTHANHTOAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ADMIN]  WITH CHECK ADD  CONSTRAINT [FK_IDADMIN_IDACCOUNT] FOREIGN KEY([MAADMIN])
REFERENCES [dbo].[ACCOUNT] ([ID])
GO
ALTER TABLE [dbo].[ADMIN] CHECK CONSTRAINT [FK_IDADMIN_IDACCOUNT]
GO
ALTER TABLE [dbo].[CHAMCONGNHANVIEN]  WITH CHECK ADD  CONSTRAINT [FK_CHAMCONGNHANVIEN_NHANVIEN] FOREIGN KEY([MANHANVIEN])
REFERENCES [dbo].[NHANVIEN] ([MANHANVIEN])
GO
ALTER TABLE [dbo].[CHAMCONGNHANVIEN] CHECK CONSTRAINT [FK_CHAMCONGNHANVIEN_NHANVIEN]
GO
ALTER TABLE [dbo].[CT_DONDATHANG]  WITH CHECK ADD  CONSTRAINT [FK_CT_DONDATHANG_DONDATHANG] FOREIGN KEY([MADON])
REFERENCES [dbo].[DONDATHANG] ([MADON])
GO
ALTER TABLE [dbo].[CT_DONDATHANG] CHECK CONSTRAINT [FK_CT_DONDATHANG_DONDATHANG]
GO
ALTER TABLE [dbo].[CT_DONDATHANG]  WITH CHECK ADD  CONSTRAINT [FK_CT_DONDATHANG_MATHANG] FOREIGN KEY([MAMATHANG])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[CT_DONDATHANG] CHECK CONSTRAINT [FK_CT_DONDATHANG_MATHANG]
GO
ALTER TABLE [dbo].[CT_DONGOIMON]  WITH CHECK ADD  CONSTRAINT [FK_CT_DONGOIMON] FOREIGN KEY([MADON])
REFERENCES [dbo].[DONGOIMON] ([MADON])
GO
ALTER TABLE [dbo].[CT_DONGOIMON] CHECK CONSTRAINT [FK_CT_DONGOIMON]
GO
ALTER TABLE [dbo].[CT_DONGOIMON]  WITH CHECK ADD  CONSTRAINT [FK_CT_DONGOIMON_MATHANG] FOREIGN KEY([MAMATHANG])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[CT_DONGOIMON] CHECK CONSTRAINT [FK_CT_DONGOIMON_MATHANG]
GO
ALTER TABLE [dbo].[CT_GIOHANG]  WITH CHECK ADD  CONSTRAINT [FK_CT_GIOHANG_KHACHHANG] FOREIGN KEY([MAKHACHHANG])
REFERENCES [dbo].[KHACHHANG] ([MAKHACHHANG])
GO
ALTER TABLE [dbo].[CT_GIOHANG] CHECK CONSTRAINT [FK_CT_GIOHANG_KHACHHANG]
GO
ALTER TABLE [dbo].[CT_GIOHANG]  WITH CHECK ADD  CONSTRAINT [FK_CT_GIOHANG_MATHANG] FOREIGN KEY([MAMATHANG])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[CT_GIOHANG] CHECK CONSTRAINT [FK_CT_GIOHANG_MATHANG]
GO
ALTER TABLE [dbo].[CT_MONAN]  WITH CHECK ADD  CONSTRAINT [FK_CT_MONAN_MONAN] FOREIGN KEY([MAMONAN])
REFERENCES [dbo].[MONAN] ([MAMONAN])
GO
ALTER TABLE [dbo].[CT_MONAN] CHECK CONSTRAINT [FK_CT_MONAN_MONAN]
GO
ALTER TABLE [dbo].[CT_MONAN]  WITH CHECK ADD  CONSTRAINT [FK_CT_MONAN_NGUYENLIEU] FOREIGN KEY([MANGUYENLIEU])
REFERENCES [dbo].[NGUYENLIEU] ([MANGUYENLIEU])
GO
ALTER TABLE [dbo].[CT_MONAN] CHECK CONSTRAINT [FK_CT_MONAN_NGUYENLIEU]
GO
ALTER TABLE [dbo].[DONDATHANG]  WITH CHECK ADD  CONSTRAINT [FK_DONDATHANG_KHACHHANG] FOREIGN KEY([MAKHACHHANG])
REFERENCES [dbo].[KHACHHANG] ([MAKHACHHANG])
GO
ALTER TABLE [dbo].[DONDATHANG] CHECK CONSTRAINT [FK_DONDATHANG_KHACHHANG]
GO
ALTER TABLE [dbo].[DUNGCU]  WITH CHECK ADD  CONSTRAINT [FK_DUNGCU_MATHANG] FOREIGN KEY([MADUNGCU])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[DUNGCU] CHECK CONSTRAINT [FK_DUNGCU_MATHANG]
GO
ALTER TABLE [dbo].[KHACHHANG]  WITH CHECK ADD  CONSTRAINT [FK_IDKHACHHANG_IDCCOUNT] FOREIGN KEY([MAKHACHHANG])
REFERENCES [dbo].[ACCOUNT] ([ID])
GO
ALTER TABLE [dbo].[KHACHHANG] CHECK CONSTRAINT [FK_IDKHACHHANG_IDCCOUNT]
GO
ALTER TABLE [dbo].[MATHANG]  WITH CHECK ADD  CONSTRAINT [FK_MATHANG_DONVITINH] FOREIGN KEY([MADONVITINH])
REFERENCES [dbo].[DONVITINH] ([MADONVITINH])
GO
ALTER TABLE [dbo].[MATHANG] CHECK CONSTRAINT [FK_MATHANG_DONVITINH]
GO
ALTER TABLE [dbo].[MATHANG]  WITH CHECK ADD  CONSTRAINT [FK_MATHANG_LOAIMATHANG] FOREIGN KEY([MALOAIMATHANG])
REFERENCES [dbo].[LOAIMATHANG] ([MALOAIMATHANG])
GO
ALTER TABLE [dbo].[MATHANG] CHECK CONSTRAINT [FK_MATHANG_LOAIMATHANG]
GO
ALTER TABLE [dbo].[MATHANGNHAP_NHACUNGCAP]  WITH CHECK ADD  CONSTRAINT [FK_MATHANGNHAP_NHACUNGCAP_MATHANG] FOREIGN KEY([MAMATHANG])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[MATHANGNHAP_NHACUNGCAP] CHECK CONSTRAINT [FK_MATHANGNHAP_NHACUNGCAP_MATHANG]
GO
ALTER TABLE [dbo].[MATHANGNHAP_NHACUNGCAP]  WITH CHECK ADD  CONSTRAINT [FK_MATHANGNHAP_NHACUNGCAP_NHACUNGCAP] FOREIGN KEY([MANHACUNGCAP])
REFERENCES [dbo].[NHACUNGCAP] ([MANHACUNGCAP])
GO
ALTER TABLE [dbo].[MATHANGNHAP_NHACUNGCAP] CHECK CONSTRAINT [FK_MATHANGNHAP_NHACUNGCAP_NHACUNGCAP]
GO
ALTER TABLE [dbo].[MENUMONAN]  WITH CHECK ADD  CONSTRAINT [FK_MENUMONAN_MONAN] FOREIGN KEY([MAMONAN])
REFERENCES [dbo].[MONAN] ([MAMONAN])
GO
ALTER TABLE [dbo].[MENUMONAN] CHECK CONSTRAINT [FK_MENUMONAN_MONAN]
GO
ALTER TABLE [dbo].[MENUNUOCGIAIKHAT]  WITH CHECK ADD  CONSTRAINT [FK_MENUNUOCGIAIKHAT_NUOCGIAIKHAT] FOREIGN KEY([MANUOCGIAIKHAT])
REFERENCES [dbo].[NUOCGIAIKHAT] ([MANUOCGIAIKHAT])
GO
ALTER TABLE [dbo].[MENUNUOCGIAIKHAT] CHECK CONSTRAINT [FK_MENUNUOCGIAIKHAT_NUOCGIAIKHAT]
GO
ALTER TABLE [dbo].[MONAN]  WITH CHECK ADD  CONSTRAINT [FK_MONAN_MATHANG] FOREIGN KEY([MAMONAN])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[MONAN] CHECK CONSTRAINT [FK_MONAN_MATHANG]
GO
ALTER TABLE [dbo].[NGUYENLIEU]  WITH CHECK ADD  CONSTRAINT [FK_NGUYENLIEU_MATHANG] FOREIGN KEY([MANGUYENLIEU])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[NGUYENLIEU] CHECK CONSTRAINT [FK_NGUYENLIEU_MATHANG]
GO
ALTER TABLE [dbo].[NHACUNGCAP]  WITH CHECK ADD  CONSTRAINT [FK_NHACUNGCAP_LOAIMATHANG] FOREIGN KEY([MALOAIMATHANGCUNGCAP])
REFERENCES [dbo].[LOAIMATHANG] ([MALOAIMATHANG])
GO
ALTER TABLE [dbo].[NHACUNGCAP] CHECK CONSTRAINT [FK_NHACUNGCAP_LOAIMATHANG]
GO
ALTER TABLE [dbo].[NHANVIEN]  WITH CHECK ADD  CONSTRAINT [FK_IDNHANVIEN_IDCCOUNT] FOREIGN KEY([MANHANVIEN])
REFERENCES [dbo].[ACCOUNT] ([ID])
GO
ALTER TABLE [dbo].[NHANVIEN] CHECK CONSTRAINT [FK_IDNHANVIEN_IDCCOUNT]
GO
ALTER TABLE [dbo].[NHANVIEN]  WITH CHECK ADD  CONSTRAINT [FK_NHANVIEN_DONVITINHLUONG] FOREIGN KEY([MADONVITINHLUONG])
REFERENCES [dbo].[DONVITINHLUONG] ([MADONVITINHLUONG])
GO
ALTER TABLE [dbo].[NHANVIEN] CHECK CONSTRAINT [FK_NHANVIEN_DONVITINHLUONG]
GO
ALTER TABLE [dbo].[NHAPMATHANG]  WITH CHECK ADD  CONSTRAINT [FK_NHAPMATHANG_MATHANG] FOREIGN KEY([MAMATHANG])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[NHAPMATHANG] CHECK CONSTRAINT [FK_NHAPMATHANG_MATHANG]
GO
ALTER TABLE [dbo].[NHAPMATHANG]  WITH CHECK ADD  CONSTRAINT [FK_NHAPMATHANG_NHACUNGCAP] FOREIGN KEY([MANHACUNGCAP])
REFERENCES [dbo].[NHACUNGCAP] ([MANHACUNGCAP])
GO
ALTER TABLE [dbo].[NHAPMATHANG] CHECK CONSTRAINT [FK_NHAPMATHANG_NHACUNGCAP]
GO
ALTER TABLE [dbo].[NUOCGIAIKHAT]  WITH CHECK ADD  CONSTRAINT [FK_NUOCGIAIKHAT_MATHANG] FOREIGN KEY([MANUOCGIAIKHAT])
REFERENCES [dbo].[MATHANG] ([MAMATHANG])
GO
ALTER TABLE [dbo].[NUOCGIAIKHAT] CHECK CONSTRAINT [FK_NUOCGIAIKHAT_MATHANG]
GO
ALTER TABLE [dbo].[THANHTOANLUONGNHANVIEN]  WITH CHECK ADD  CONSTRAINT [FK_THANHTOANLUONGNHANVIEN_NHANVIEN] FOREIGN KEY([MANHANVIEN])
REFERENCES [dbo].[NHANVIEN] ([MANHANVIEN])
GO
ALTER TABLE [dbo].[THANHTOANLUONGNHANVIEN] CHECK CONSTRAINT [FK_THANHTOANLUONGNHANVIEN_NHANVIEN]
GO
ALTER TABLE [dbo].[ACCOUNT]  WITH CHECK ADD  CONSTRAINT [LENGHT_HOTEN] CHECK  ((len([HOTEN])>(3)))
GO
ALTER TABLE [dbo].[ACCOUNT] CHECK CONSTRAINT [LENGHT_HOTEN]
GO
ALTER TABLE [dbo].[ACCOUNT]  WITH CHECK ADD  CONSTRAINT [LENGHT_MATKHAU] CHECK  ((len([MATKHAU])>(5)))
GO
ALTER TABLE [dbo].[ACCOUNT] CHECK CONSTRAINT [LENGHT_MATKHAU]
GO
ALTER TABLE [dbo].[ACCOUNT]  WITH CHECK ADD  CONSTRAINT [LENGHT_TAIKHOAN] CHECK  ((len([TAIKHOAN])>(5)))
GO
ALTER TABLE [dbo].[ACCOUNT] CHECK CONSTRAINT [LENGHT_TAIKHOAN]
GO
USE [master]
GO
ALTER DATABASE [DBQUANLYQUANAN] SET  READ_WRITE 
GO
