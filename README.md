**ĐỒ ÁN MÔN CÔNG NGHỆ .NET - (Lớp: SE310.K22)**

**ĐỀ TÀI: WEBSITE QUẢN LÝ QUÁN ĂN**

**SINH VIÊN THỰC HIỆN:**

- Nguyễn Anh Tấn - 17521013

- Lộc Đức Thắng - 17521039

- Nguyễn Văn Tuấn - 17521218

- Lê Hoàng Long – 17520709

**GIẢNG VIÊN HƯỚNG DẪN:**

Cô Nguyễn Thị Thanh Trúc

**GIẢNG VIÊN THỰC HÀNH:**

Cô Trần Hạnh Xuân



**GIỚI THIỆU ĐỀ TÀI**

Hiện nay, dưới thời kỳ công nghệ thông tin đang bùng nổ rất mạnh, hầu hết các doanh nghiệp từ lớn đến nhỏ đều áp dụng công nghệ vào việc quản lý, xử lý các công việc hàng ngày để tối ưu công việc. Và các quán ăn cũng vậy! Việc áp dụng công nghệ vào quản lý quán ăn giúp chủ quán có được các số liệu thống kê cụ thể về thu chi, quản lý dữ liệu về hàng hóa, nhân viên, khách hàng… một cách chính xác và hiệu quả.

Trong đồ án này, nhóm em sẽ khảo sát hiện trạng và tiến hành xây dựng website quản lý quán ăn cho quán cơm Bầu Bí.


Chi tiết xem tại: [Documents](https://gitlab.com/anhtannguyen1999/quanlyquananasp/-/tree/master/Documents)